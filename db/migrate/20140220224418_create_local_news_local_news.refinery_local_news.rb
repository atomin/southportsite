# This migration comes from refinery_local_news (originally 1)
class CreateLocalNewsLocalNews < ActiveRecord::Migration

  def up
    create_table :refinery_local_news do |t|
      t.string :title
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-local_news"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/local_news/local_news"})
    end

    drop_table :refinery_local_news

  end

end
