module Refinery
  module Articles
    class Article < Refinery::Core::BaseModel
      self.table_name = 'refinery_articles'

      attr_accessible :title, :date, :short, :body, :position

      validates :title, :presence => true, :uniqueness => true
    end
  end
end
