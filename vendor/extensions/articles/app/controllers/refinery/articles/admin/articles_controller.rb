module Refinery
  module Articles
    module Admin
      class ArticlesController < ::Refinery::AdminController

        crudify :'refinery/articles/article',
                :xhr_paging => true,
                :order => "date DESC",
                :sortable => false

      end
    end
  end
end
